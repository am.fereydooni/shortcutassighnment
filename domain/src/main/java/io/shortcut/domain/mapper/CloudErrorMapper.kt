package io.shortcut.domain.mapper

import io.shortcut.domain.model.DomainErrorException

interface CloudErrorMapper {

    fun mapToDomainErrorException(throwable: Throwable?): DomainErrorException
}