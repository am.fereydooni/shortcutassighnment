package io.shortcut.domain.mapper

import io.shortcut.domain.model.ErrorModel
import io.shortcut.domain.model.ErrorStatus
import javax.inject.Inject

class DomainErrorUtil @Inject constructor(private val cloudErrorMapper: CloudErrorMapper) {

    fun getErrorModel(t: Throwable?): ErrorModel {
        if (t is NullPointerException) {
            return ErrorModel(
                ErrorStatus.EMPTY_RESPONSE
            )
        }

        return cloudErrorMapper.mapToDomainErrorException(t).errorModel
    }
}