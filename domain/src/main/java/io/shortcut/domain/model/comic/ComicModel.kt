package io.shortcut.domain.model.comic

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

private const val TABLE_NAME = "favorite"

@Entity(tableName = TABLE_NAME)
data class ComicModel(
    @ColumnInfo
    val alt: String,
    @ColumnInfo
    val day: String,
    @ColumnInfo
    val img: String,
    @ColumnInfo
    val link: String,
    @ColumnInfo
    val month: String,
    @ColumnInfo
    val news: String,
    @PrimaryKey
    @ColumnInfo
    val num: Int,
    @ColumnInfo
    val safe_title: String,
    @ColumnInfo
    val title: String,
    @ColumnInfo
    val transcript: String,
    @ColumnInfo
    val year: String
)