package io.shortcut.domain.model

data class ErrorModel(
    val code: Int? = null,
    val message: String?,
    @Transient var errorStatus: ErrorStatus
) {
    constructor(errorStatus: ErrorStatus) : this(null, null, errorStatus)
}