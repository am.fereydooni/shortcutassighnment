package io.shortcut.domain.model

class DomainErrorException(val errorModel: ErrorModel) : Throwable()