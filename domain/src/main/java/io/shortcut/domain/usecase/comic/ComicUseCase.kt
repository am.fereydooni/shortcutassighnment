package io.shortcut.domain.usecase.comic

import io.reactivex.Flowable
import io.shortcut.domain.mapper.DomainErrorUtil
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.ComicRepository
import io.shortcut.domain.usecase.base.FlowableUseCase
import javax.inject.Inject

class ComicUseCase @Inject constructor(
    private val comicRepository: ComicRepository,
    errorUtil: DomainErrorUtil
) : FlowableUseCase<ComicModel>(errorUtil) {

    override fun execute(): Flowable<ComicModel> {
        return comicRepository.getLastComic()
    }
}