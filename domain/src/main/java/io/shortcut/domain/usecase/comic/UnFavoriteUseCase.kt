package io.shortcut.domain.usecase.comic

import io.reactivex.Flowable
import io.shortcut.domain.mapper.DomainErrorUtil
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.FavoriteRepository
import io.shortcut.domain.usecase.base.FlowableUseCase
import javax.inject.Inject

class UnFavoriteUseCase @Inject constructor(
    errorUtil: DomainErrorUtil,
    private val favoriteRepository: FavoriteRepository
) : FlowableUseCase<Unit>(errorUtil) {

    private lateinit var comicModel: ComicModel

    fun setParameters(comicModel: ComicModel): UnFavoriteUseCase {
        this.comicModel = comicModel
        return this
    }

    override fun execute(): Flowable<Unit> {
        return favoriteRepository.unFavoriteComic(comicModel)
    }

}