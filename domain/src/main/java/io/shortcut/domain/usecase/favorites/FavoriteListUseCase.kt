package io.shortcut.domain.usecase.favorites

import io.reactivex.Flowable
import io.shortcut.domain.mapper.DomainErrorUtil
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.FavoriteListRepository
import io.shortcut.domain.usecase.base.FlowableUseCase
import javax.inject.Inject

class FavoriteListUseCase @Inject constructor(
    errorUtil: DomainErrorUtil,
    private val favoriteListRepository: FavoriteListRepository
) : FlowableUseCase<List<ComicModel>>(errorUtil) {

    override fun execute(): Flowable<List<ComicModel>> {
        return favoriteListRepository.getFavoriteListComic()
    }
}