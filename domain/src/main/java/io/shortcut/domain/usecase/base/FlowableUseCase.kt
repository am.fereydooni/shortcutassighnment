package io.shortcut.domain.usecase.base

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.shortcut.domain.mapper.DomainErrorUtil
import io.shortcut.domain.model.ErrorResponse
import io.shortcut.domain.model.SuccessResponse
import io.shortcut.domain.model.UseCaseResponse

abstract class FlowableUseCase<T>(val errorUtil: DomainErrorUtil) : UseCase<Flowable<T>>() {
    fun execute(
        compositeDisposable: CompositeDisposable,
        onResponse: (UseCaseResponse<T>) -> Unit
    ): Disposable {
        return this.execute()
            .onBackpressureLatest()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    onResponse(
                        SuccessResponse(
                            it
                        )
                    )
                },
                {
                    val error = errorUtil.getErrorModel(it)
                    onResponse(
                        ErrorResponse(
                            error
                        )
                    )
                }).also { compositeDisposable.add(it) }
    }
}