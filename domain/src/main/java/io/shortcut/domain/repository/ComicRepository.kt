package io.shortcut.domain.repository

import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

interface ComicRepository {

    fun getLastComic(): Flowable<ComicModel>
    fun getComicByNum(num: Int): Flowable<ComicModel>

}