package io.shortcut.domain.repository

import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

interface FavoriteListRepository {

    fun getFavoriteListComic(): Flowable<List<ComicModel>>

}