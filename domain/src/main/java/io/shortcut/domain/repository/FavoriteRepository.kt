package io.shortcut.domain.repository

import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

interface FavoriteRepository {

    fun favoriteComic(comicModel: ComicModel): Flowable<Unit>

    fun unFavoriteComic(comicModel: ComicModel): Flowable<Unit>

    fun isFavoriteComic(comicModel: ComicModel): Flowable<Int>

}