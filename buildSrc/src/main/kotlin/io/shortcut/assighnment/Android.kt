package io.shortcut.assighnment

object Android {
    const val minSdkVersion = 21
    const val targetSdkVersion = 31
    const val compileSdkVersion = 31
    const val applicationId = "io.shortcut.assighnment"
    const val versionCode = 1
    const val versionName = "0.1"
}