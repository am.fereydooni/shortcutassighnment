# Getting Started
Shortcut Assignment Version 1.0 for android devices .

### Technologies Used
* Clean architecture
* Alongside MVVM
* Kotlin Language
* Dagger2
* Retrofit2
* RX Java2
* RX Android
* Glide
* Room runtime
* Unit Test

### Features
* Browsing comics
* Searching comic by number
* Opening the explanation of comics
* Making comics as favorite and accessing the favorite list

### Installation
* Import projects on android studio
* Set skd path for the project
* Build and run the app

### Reference Documentation
For further reference, please consider the following sections:

* https://developer.android.com/docs
* https://developer.android.com/jetpack/guide
* https://www.tutorialspoint.com/kotlin/index.htm

