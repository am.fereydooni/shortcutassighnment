package io.shortcut.assighnment.ui.favorite

import io.shortcut.assighnment.ui.favoite.FavoriteListViewModel
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.usecase.comic.UnFavoriteUseCase
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class FavoriteListViewModelTest {

    private val unFavoriteUseCase: UnFavoriteUseCase = mock()
    private val viewModel = FavoriteListViewModel(unFavoriteUseCase)

    @Test
    fun `an item should un-favorite when onItemClick is called`() {
        // GIVEN
        val item: ComicModel = mock()
        whenever(unFavoriteUseCase.setParameters(any())).doReturn(unFavoriteUseCase)

        // WHEN
        viewModel.onItemClicked(item)

        // THEN
        verify(unFavoriteUseCase).execute(any(), any())
    }
}
