package io.shortcut.assighnment.app

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.shortcut.assighnment.di.component.DaggerAppComponent

class ShortcutApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.factory().create(this)
}