package io.shortcut.assighnment.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.shortcut.data.source.db.AppDataBase
import io.shortcut.data.source.db.dao.FavoriteDao
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): AppDataBase {
        return Room
            .databaseBuilder(context, AppDataBase::class.java, AppDataBase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideXmlFeedDao(appDataBase: AppDataBase): FavoriteDao {
        return appDataBase.favoriteDao()
    }

}