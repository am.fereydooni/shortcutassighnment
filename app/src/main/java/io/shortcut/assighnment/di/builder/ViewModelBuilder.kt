package io.shortcut.assighnment.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.shortcut.assighnment.di.ShortcutViewModelFactory
import io.shortcut.assighnment.ui.comic.SingleComicViewModel
import io.shortcut.assighnment.ui.favoite.FavoriteListViewModel
import io.shortcut.assighnment.ui.main.MainViewModel


@Module
abstract class ViewModelBuilder {

    @Binds
    abstract fun bindShortcutViewModelFactory(shortcutViewModelFactory: ShortcutViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(maniViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SingleComicViewModel::class)
    abstract fun bindSingleComicViewModel(singleComicViewModel: SingleComicViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteListViewModel::class)
    abstract fun bindFavoriteListViewModel(favoriteListViewModel: FavoriteListViewModel): ViewModel
}