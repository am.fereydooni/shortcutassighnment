package io.shortcut.assighnment.di.module

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.shortcut.assighnment.BuildConfig
import io.shortcut.assighnment.util.SecretFields
import io.shortcut.data.di.qualifier.Cloud
import io.shortcut.data.di.qualifier.Mock
import io.shortcut.data.restful.APIs
import io.shortcut.data.source.cloud.BaseCloudRepository
import io.shortcut.data.source.cloud.CloudMockRepository
import io.shortcut.data.source.cloud.CloudRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {

    /**
     * provides Gson
     */
    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    /**
     * provides instance of [OkHttpClient] for without-token api services
     *
     * @param headers default shared headers provided by [provideSharedHeaders]
     * @return an instance of [OkHttpClient]
     */
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
            builder.addNetworkInterceptor(StethoInterceptor())
        }
        builder.connectTimeout(20L, TimeUnit.SECONDS)
        builder.readTimeout(20L, TimeUnit.SECONDS)
        builder.writeTimeout(20L, TimeUnit.SECONDS)
        return builder.build()
    }

    /**
     * provide an instance of [Retrofit] for without-token api services
     *
     * @param okHttpClient an instance of without-token [okHttpClient] provided by [provideOkHttpClient]
     * @param gson an instance of gson provided by [provideGson] to use as retrofit converter factory
     *
     * @return an instance of [Retrofit] for without-token api calls
     */
    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder().client(okHttpClient)
            // create gson converter factory
            .addConverterFactory(GsonConverterFactory.create(gson))
            // create call adapter factory for RxJava
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            // get base url from SecretFields interface
            .baseUrl(SecretFields.getBaseURL())
            .build()
    }


    /**
     * provides [APIs] service to use for without-token api calls
     *
     * @param retrofit an instance of without-token [Retrofit]
     *
     * @return returns an instance of [APIs]
     */
    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): APIs {
        return retrofit.create(APIs::class.java)
    }

    /**
     * provides real implementation of [BaseCloudRepository] to access real api services
     *
     * @param apIs an instance of [APIs] to access all without-token apis
     *
     * @return returns an instance of [CloudRepository]
     */
    @Cloud
    @Provides
    fun provideCloudRepository(apIs: APIs): BaseCloudRepository {
        return CloudRepository(apIs)
    }

    /**
     * provides mock implementation of [BaseCloudRepository] to access mock api services
     *
     * @return returns an instance of [CloudMockRepository]
     */
    @Mock
    @Provides
    fun provideCloudMockRepository(apIs: APIs, gson: Gson): BaseCloudRepository {
        return CloudMockRepository(gson)
    }
}