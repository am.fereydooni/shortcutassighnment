package io.shortcut.assighnment.di.builder

import dagger.Binds
import dagger.Module
import io.shortcut.data.mapper.CloudErrorMapperImpl
import io.shortcut.data.repository.ComicRepositoryImpl
import io.shortcut.data.repository.FavoriteListRepositoryImpl
import io.shortcut.data.repository.FavoriteRepositoryImpl
import io.shortcut.domain.mapper.CloudErrorMapper
import io.shortcut.domain.repository.ComicRepository
import io.shortcut.domain.repository.FavoriteListRepository
import io.shortcut.domain.repository.FavoriteRepository

@Module
abstract class RepositoryBuilder {

    @Binds
    abstract fun bindsCloudErrorMapper(cloudErrorMapperImpl: CloudErrorMapperImpl): CloudErrorMapper

    @Binds
    abstract fun bindComicRepo(comicRepositoryImpl: ComicRepositoryImpl): ComicRepository

    @Binds
    abstract fun bindFavoriteRepository(jsonFeedRepositoryImpl: FavoriteRepositoryImpl): FavoriteRepository

    @Binds
    abstract fun bindFavoriteListRepository(jsonFeedRepositoryImpl: FavoriteListRepositoryImpl): FavoriteListRepository
}