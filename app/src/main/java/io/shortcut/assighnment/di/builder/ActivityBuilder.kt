package io.shortcut.assighnment.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.shortcut.assighnment.ui.main.MainActivity
import io.shortcut.assighnment.ui.main.MainFragmentProvider

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainFragmentProvider::class)])
    internal abstract fun bindMainActivity(): MainActivity
}