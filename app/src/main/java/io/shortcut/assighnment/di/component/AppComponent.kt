package io.shortcut.assighnment.di.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.shortcut.assighnment.app.ShortcutApplication
import io.shortcut.assighnment.di.builder.ActivityBuilder
import io.shortcut.assighnment.di.builder.RepositoryBuilder
import io.shortcut.assighnment.di.module.AppModule
import io.shortcut.assighnment.di.module.DatabaseModule
import io.shortcut.assighnment.di.module.NetworkModule
import javax.inject.Singleton

/**
 * Main Application [Component] that included all of modules and sub components.
 */
@Singleton
@Component(
    modules = [
        RepositoryBuilder::class,
        NetworkModule::class,
        AppModule::class,
        ActivityBuilder::class,
        AndroidSupportInjectionModule::class,
        DatabaseModule::class
    ]
)


interface AppComponent : AndroidInjector<ShortcutApplication> {

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<ShortcutApplication> {
        interface Factory {
            fun create(@BindsInstance application: Context): AppComponent
        }
    }
}