package io.shortcut.assighnment.util

object SecretFields {

    fun getBaseURL(): String = "https://xkcd.com/"
    fun getExplanationURL(): String = "https://explainxkcd.com/"
}