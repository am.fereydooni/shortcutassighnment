package io.shortcut.assighnment.ui.favoite

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import io.shortcut.assighnment.R
import io.shortcut.assighnment.base.BaseFragment
import io.shortcut.assighnment.databinding.FragmentFavoriteListBinding
import io.shortcut.assighnment.databinding.ItemFavoriteBinding
import io.shortcut.assighnment.ui.adapter.SingleLayoutAdapter
import io.shortcut.assighnment.ui.main.MainViewModel
import io.shortcut.domain.model.comic.ComicModel

class FavoriteListFragment : BaseFragment<FavoriteListViewModel, FragmentFavoriteListBinding>() {

    override val viewModel: FavoriteListViewModel by viewModels { viewModelFactory }
    override val layoutId: Int = R.layout.fragment_favorite_list

    private val sharedViewModel: MainViewModel by activityViewModels { viewModelFactory }

    override fun onViewInitialized() {
        super.onViewInitialized()
        binding.viewModel = viewModel
        binding.adapter = SingleLayoutAdapter<ComicModel, ItemFavoriteBinding>(
            layoutId = R.layout.item_favorite,
            viewModel = viewModel
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
    }

    private fun addObservers() {
        sharedViewModel.favoriteListLiveData.observe(viewLifecycleOwner) {
            binding.adapter?.submitList(it)
        }
        sharedViewModel.error.observe(viewLifecycleOwner, {
            Toast.makeText(context, "${it?.errorStatus}", Toast.LENGTH_LONG).show()
        })
        viewModel.error.observe(viewLifecycleOwner, {
            Toast.makeText(context, "${it?.errorStatus}", Toast.LENGTH_LONG).show()
        })
    }
}