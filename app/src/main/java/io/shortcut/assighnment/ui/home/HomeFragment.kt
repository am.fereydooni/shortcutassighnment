package io.shortcut.assighnment.ui.home

import androidx.fragment.app.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import io.shortcut.assighnment.R
import io.shortcut.assighnment.base.BaseFragment
import io.shortcut.assighnment.databinding.FragmentMainBinding
import io.shortcut.assighnment.ui.viewpager.ViewPagerAdapter

class HomeFragment : BaseFragment<HomeViewModel, FragmentMainBinding>() {

    override val viewModel: HomeViewModel by viewModels { viewModelFactory }
    override val layoutId: Int = R.layout.fragment_main
    lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun onViewInitialized() {
        super.onViewInitialized()
        setupViewPager()
    }

    fun setupViewPager() {
        viewPagerAdapter = ViewPagerAdapter(this)
        val viewPager = binding.viewPager
        viewPager.adapter = viewPagerAdapter
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            viewPager.setCurrentItem(tab.position, true)
            when (position) {
                0 -> tab.text = "Comic"
                1 -> tab.text = "Favorites"
            }
        }.attach()
    }
}