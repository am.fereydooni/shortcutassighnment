package io.shortcut.assighnment.ui.main

import androidx.activity.viewModels
import io.shortcut.assighnment.R
import io.shortcut.assighnment.base.BaseActivity
import io.shortcut.assighnment.databinding.ActivityMainBinding

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>() {

    override val viewModel: MainViewModel by viewModels { viewModelFactory }
    override val layoutId: Int = R.layout.activity_main
}