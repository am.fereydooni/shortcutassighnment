package io.shortcut.assighnment.ui.favoite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.shortcut.assighnment.base.BaseViewModel
import io.shortcut.domain.model.ErrorModel
import io.shortcut.domain.model.ErrorResponse
import io.shortcut.domain.model.SuccessResponse
import io.shortcut.domain.model.UseCaseResponse
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.usecase.comic.UnFavoriteUseCase
import javax.inject.Inject

class FavoriteListViewModel @Inject constructor(private val unFavoriteUseCase: UnFavoriteUseCase) :
    BaseViewModel() {

    private val _error = MutableLiveData<ErrorModel>()
    val error: LiveData<ErrorModel> get() = _error

    val loadingLiveData = MutableLiveData<Boolean>()

    fun onItemClicked(item: ComicModel) {
        unFavoriteComic(item)
    }

    private fun unFavoriteComic(comicModel: ComicModel) {
        unFavoriteUseCase.setParameters(comicModel).execute(
            this.compositeDisposable, this::onUnFavoriteResponse
        )
    }

    private fun onUnFavoriteResponse(response: UseCaseResponse<Unit>) {
        when (response) {
            is SuccessResponse -> {}
            is ErrorResponse -> _error.value = response.error
        }
    }
}