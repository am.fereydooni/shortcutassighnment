package io.shortcut.assighnment.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.shortcut.assighnment.ui.comic.SingleComicFragment
import io.shortcut.assighnment.ui.favoite.FavoriteListFragment
import io.shortcut.assighnment.ui.home.HomeFragment

@Module
abstract class MainFragmentProvider {

    @ContributesAndroidInjector
    abstract fun provideMainFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun provideSingleComicFragment(): SingleComicFragment

    @ContributesAndroidInjector
    abstract fun provideFavoriteListFragment(): FavoriteListFragment
}