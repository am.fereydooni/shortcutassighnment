package io.shortcut.assighnment.ui.viewpager

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.shortcut.assighnment.ui.comic.SingleComicFragment
import io.shortcut.assighnment.ui.favoite.FavoriteListFragment

class ViewPagerAdapter constructor(fragment: Fragment) :
    FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return SingleComicFragment()
            1 -> return FavoriteListFragment()
        }
        return SingleComicFragment()
    }
}
