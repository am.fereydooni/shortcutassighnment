package io.shortcut.assighnment.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.shortcut.assighnment.base.BaseViewModel
import io.shortcut.assighnment.util.livedata.SingleEventLiveData
import io.shortcut.domain.model.ErrorModel
import io.shortcut.domain.model.ErrorResponse
import io.shortcut.domain.model.SuccessResponse
import io.shortcut.domain.model.UseCaseResponse
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.usecase.favorites.FavoriteListUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val favoriteListUseCase: FavoriteListUseCase
) : BaseViewModel() {

    val _favoriteListLiveData = SingleEventLiveData<List<ComicModel>>()
    val favoriteListLiveData: LiveData<List<ComicModel>> get() = _favoriteListLiveData

    val _error = MutableLiveData<ErrorModel>()
    val error: LiveData<ErrorModel> get() = _error

    init {
        getFavoriteList()
    }

    private fun getFavoriteList() {
        favoriteListUseCase.execute(compositeDisposable, this::onGetFavoriteList)
    }

    private fun onGetFavoriteList(response: UseCaseResponse<List<ComicModel>>) {
        when (response) {
            is SuccessResponse -> _favoriteListLiveData.value = response.value
            is ErrorResponse -> _error.value = response.error
        }
    }
}