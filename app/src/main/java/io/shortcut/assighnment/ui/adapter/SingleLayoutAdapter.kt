package io.shortcut.assighnment.ui.adapter

import androidx.databinding.ViewDataBinding
import io.shortcut.assighnment.base.BaseViewModel

open class SingleLayoutAdapter<T : Any, B : ViewDataBinding>(
    private val layoutId: Int,
    items: List<T> = emptyList(),
    viewModel: BaseViewModel? = null,
    onBind: B.(Int) -> Unit = {}
) : BaseAdapter<T, B>(items = items, onBind = onBind, viewModel = viewModel) {

    override fun getLayoutId(position: Int): Int = layoutId
}