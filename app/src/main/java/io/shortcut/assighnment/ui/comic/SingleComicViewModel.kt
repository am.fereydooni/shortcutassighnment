package io.shortcut.assighnment.ui.comic

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import io.shortcut.assighnment.base.BaseViewModel
import io.shortcut.assighnment.util.livedata.SingleEventLiveData
import io.shortcut.domain.model.ErrorModel
import io.shortcut.domain.model.ErrorResponse
import io.shortcut.domain.model.SuccessResponse
import io.shortcut.domain.model.UseCaseResponse
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.usecase.comic.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class SingleComicViewModel @Inject constructor(
    private val comicUseCase: ComicUseCase,
    private val favoriteUseCase: FavoriteUseCase,
    private val unFavoriteUseCase: UnFavoriteUseCase,
    private val isFavoriteUseCase: IsFavoriteUseCase,
    private val searchComicByNumberUseCase: SearchComicByNumberUseCase
) :
    BaseViewModel() {

    private val _comicData = MutableLiveData<ComicModel>()
    val comicData: LiveData<ComicModel> get() = _comicData

    private val _error = MutableLiveData<ErrorModel>()
    val error: LiveData<ErrorModel> get() = _error

    private val _eventShareComic = SingleEventLiveData<String>()
    val eventShareComic: LiveData<String> get() = _eventShareComic

    private val _evenComicExplanation = SingleEventLiveData<String>()
    val evenComicExplanation: LiveData<String> get() = _evenComicExplanation

    private val _isFavorite = SingleEventLiveData<Int>()
    val isFavorite: LiveData<Int> get() = _isFavorite

    private val _doneFavorite = SingleEventLiveData<Boolean>()
    val doneFavorite: LiveData<Boolean> get() = _doneFavorite

    private val _doneUnFavorite = SingleEventLiveData<Boolean>()
    val doneUnFavorite: LiveData<Boolean> get() = _doneUnFavorite

    val subject: PublishSubject<String> by lazy { PublishSubject.create<String>() }

    private val _debounceLiveData = SingleEventLiveData<String>()
    val debounceLiveData: LiveData<String> get() = _debounceLiveData

    init {
        getLastComic()
        searchDebounce()
    }

    fun getLastComic() {
        comicUseCase.execute(compositeDisposable, this::onGetLastComic)
    }

    fun onGetLastComic(response: UseCaseResponse<ComicModel>) {
        when (response) {
            is SuccessResponse -> {
//                _comicData.value = response.value
//                isFavoriteComic(response.value)
                val random = Random(System.nanoTime()).nextInt(response.value.num)
                getComicByNumber(random)
            }
            is ErrorResponse -> _error.value = response.error
        }
    }

    private fun getComicByNumber(num: Int) {
        searchComicByNumberUseCase.setParameters(num)
            .execute(compositeDisposable, this::onGetComicByNum)
    }

    private fun onGetComicByNum(response: UseCaseResponse<ComicModel>) {
        when (response) {
            is SuccessResponse -> {
                _comicData.value = response.value
                isFavoriteComic(response.value)
            }
            is ErrorResponse -> _error.value = response.error
        }
    }

    fun favoriteAction() {
        when (isFavorite.value) {
            1 -> unFavoriteComic(comicData.value!!)
            0 -> favoriteComic(comicData.value!!)
        }
    }

    fun shareComic() {
        _eventShareComic.value = CALL_SINGLE_EVENT
    }

    fun displayComicExplanation() {
        _evenComicExplanation.value = CALL_SINGLE_EVENT
    }

    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        _debounceLiveData.value = s.toString()
    }

    @SuppressLint("CheckResult")
    fun searchDebounce() {
        subject.debounce(1000, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                getComicByNumber(if (!it.equals("")) Integer.parseInt(it) else comicData.value!!.num)
            }
            .also {
                compositeDisposable.add(it)
            }
    }

    fun doneFavoriteAction(done: Int) {
        _isFavorite.value = done
    }

    private fun isFavoriteComic(comicModel: ComicModel) {
        isFavoriteUseCase.setParameters(comicModel).execute(
            this.compositeDisposable, this::onIsFavoriteResponse
        )
    }

    private fun onIsFavoriteResponse(response: UseCaseResponse<Int>) {
        when (response) {
            is SuccessResponse -> _isFavorite.value = response.value
            is ErrorResponse -> _error.value = response.error
        }
    }

    private fun favoriteComic(comicModel: ComicModel) {
        favoriteUseCase.setParameters(comicModel).execute(
            this.compositeDisposable, this::onFavoriteResponse
        )
    }

    private fun onFavoriteResponse(response: UseCaseResponse<Unit>) {
        when (response) {
            is SuccessResponse -> _doneFavorite.value = true
            is ErrorResponse -> _error.value = response.error
        }
    }

    private fun unFavoriteComic(comicModel: ComicModel) {
        unFavoriteUseCase.setParameters(comicModel).execute(
            this.compositeDisposable, this::onUnFavoriteResponse
        )
    }

    private fun onUnFavoriteResponse(response: UseCaseResponse<Unit>) {
        when (response) {
            is SuccessResponse -> _doneUnFavorite.value = true
            is ErrorResponse -> _error.value = response.error
        }
    }
}