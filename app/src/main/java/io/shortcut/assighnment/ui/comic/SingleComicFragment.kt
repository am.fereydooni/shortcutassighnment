package io.shortcut.assighnment.ui.comic

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import io.shortcut.assighnment.R
import io.shortcut.assighnment.base.BaseFragment
import io.shortcut.assighnment.databinding.FragmentSingleComicBinding
import io.shortcut.assighnment.util.SecretFields
import io.shortcut.domain.model.comic.ComicModel
import kotlinx.android.synthetic.main.fragment_single_comic.*

class SingleComicFragment : BaseFragment<SingleComicViewModel, FragmentSingleComicBinding>() {

    override val viewModel: SingleComicViewModel by viewModels { viewModelFactory }
    override val layoutId: Int = R.layout.fragment_single_comic
    lateinit var query: String

    override fun onViewInitialized() {
        super.onViewInitialized()
        binding.vm = viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        query = edit_query.text.toString()
        addObservers()
    }

    private fun initView(it: ComicModel?) {
        binding.data = it
    }

    private fun addObservers() {
        with(viewModel) {
            comicData.observe(viewLifecycleOwner, {
                initView(it)
            })

            error.observe(viewLifecycleOwner, {
                Toast.makeText(context, "${it?.errorStatus}", Toast.LENGTH_LONG).show()
            })
            isFavorite.observe(viewLifecycleOwner, {
                when (it) {
                    1 -> btn_favorite.setImageResource(R.drawable.ic_star_selected)
                    0 -> btn_favorite.setImageResource(R.drawable.ic_star_not_selected)
                }
            })

            doneFavorite.observe(viewLifecycleOwner, {
                btn_favorite.setImageResource(R.drawable.ic_star_selected)
                doneFavoriteAction(1)
            })

            doneUnFavorite.observe(viewLifecycleOwner, {
                btn_favorite.setImageResource(R.drawable.ic_star_selected)
                doneFavoriteAction(0)
            })

            eventShareComic.observe(viewLifecycleOwner, {
                shareComic(comicData.value!!)
            })

            evenComicExplanation.observe(viewLifecycleOwner, {
                openDescriptionPage(comicData.value!!.num)
            })

            debounceLiveData.observe(viewLifecycleOwner, {
                if (query != it) {
                    query = it
                    subject.onNext(it)
                }
            })
        }
    }

    //text format of comic for sharing
    private fun createShareText(comicModel: ComicModel): String {
        return SecretFields.getExplanationURL() + comicModel.num
    }

    private fun shareComic(comicModel: ComicModel) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, createShareText(comicModel));
        startActivity(Intent.createChooser(shareIntent, "Share To:"))
    }


    private fun openDescriptionPage(num: Int) {
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(SecretFields.getExplanationURL() + num))
        startActivity(browserIntent)
    }
}