package io.shortcut.data.source.cloud

import io.reactivex.Flowable
import io.shortcut.data.restful.APIs
import io.shortcut.domain.model.comic.ComicModel

class CloudRepository(private val api: APIs) : BaseCloudRepository {

    override fun getLatestComic(): Flowable<ComicModel> {
        return api.getLastComic()
    }

    override fun getComicByNumber(num: Int): Flowable<ComicModel> {
        return api.getComicByNumber(num)
    }
}