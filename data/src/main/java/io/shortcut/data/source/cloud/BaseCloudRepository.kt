package io.shortcut.data.source.cloud

import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

interface BaseCloudRepository {

    fun getLatestComic(): Flowable<ComicModel>

    fun getComicByNumber(num: Int): Flowable<ComicModel>


}