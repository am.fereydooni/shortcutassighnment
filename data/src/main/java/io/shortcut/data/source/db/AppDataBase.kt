package io.shortcut.data.source.db

import androidx.room.Database
import androidx.room.RoomDatabase
import io.shortcut.data.source.db.dao.FavoriteDao
import io.shortcut.domain.model.comic.ComicModel

@Database(
    entities = [ComicModel::class], version = AppDataBase.VERSION
)
abstract class AppDataBase : RoomDatabase() {
    companion object {
        const val DB_NAME = "shortcut.assignment"
        const val VERSION = 1
    }

    abstract fun favoriteDao(): FavoriteDao
}