package io.shortcut.data.source.cloud

import com.google.gson.Gson
import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

class CloudMockRepository(gson: Gson) : BaseCloudRepository {

    override fun getLatestComic(): Flowable<ComicModel> {
        TODO("Not yet implemented")
    }

    override fun getComicByNumber(num: Int): Flowable<ComicModel> {
        TODO("Not yet implemented")
    }

}