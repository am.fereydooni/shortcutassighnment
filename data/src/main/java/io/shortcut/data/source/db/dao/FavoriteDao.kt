package io.shortcut.data.source.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel

@Dao
interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun favoriteComic(comicModel: ComicModel)

    @Query("DELETE FROM favorite WHERE num = :num")
    fun unFavoriteJsonFeed(num: Int)

    @Query("SELECT EXISTS(SELECT 1 FROM favorite WHERE num = :num LIMIT 1)")
    fun isFavoriteComic(num: Int): Int

    @Query("SELECT * FROM favorite")
    fun allFavorites(): Flowable<List<ComicModel>>

}