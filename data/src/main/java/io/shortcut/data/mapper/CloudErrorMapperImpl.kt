package io.shortcut.data.mapper

import com.google.gson.Gson
import io.shortcut.domain.mapper.CloudErrorMapper
import io.shortcut.domain.model.DomainErrorException
import io.shortcut.domain.model.ErrorModel
import io.shortcut.domain.model.ErrorStatus
import java.net.SocketTimeoutException
import javax.inject.Inject

class CloudErrorMapperImpl @Inject constructor(private val gson: Gson) : CloudErrorMapper {

    override fun mapToDomainErrorException(throwable: Throwable?): DomainErrorException {
        val errorModel: ErrorModel = when (throwable) {

            is SocketTimeoutException -> {
                ErrorModel(
                    errorStatus = ErrorStatus.TIMEOUT,
                    message = throwable?.message.orEmpty()
                )
            }

            else -> {
                ErrorModel(
                    errorStatus = ErrorStatus.NO_CONNECTION,
                    message = throwable?.message.orEmpty()
                )
            }
        }
        return DomainErrorException(errorModel)
    }
}