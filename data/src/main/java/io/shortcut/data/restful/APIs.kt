package io.shortcut.data.restful

import io.reactivex.Flowable
import io.shortcut.domain.model.comic.ComicModel
import retrofit2.http.GET
import retrofit2.http.Path

interface APIs {

    @GET("info.0.json")
    fun getLastComic(): Flowable<ComicModel>

    @GET("{number}/info.0.json")
    fun getComicByNumber(@Path("number") number: Int): Flowable<ComicModel>
}