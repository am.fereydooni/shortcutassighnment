package io.shortcut.data.repository

import io.reactivex.Flowable
import io.shortcut.data.source.db.dao.FavoriteDao
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.FavoriteListRepository
import javax.inject.Inject

class FavoriteListRepositoryImpl @Inject constructor(
    private val favoriteDao: FavoriteDao
) : FavoriteListRepository {

    override fun getFavoriteListComic(): Flowable<List<ComicModel>> {
        return favoriteDao.allFavorites()
    }

}