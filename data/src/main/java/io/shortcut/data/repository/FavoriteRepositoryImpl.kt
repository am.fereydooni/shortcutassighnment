package io.shortcut.data.repository

import io.reactivex.Flowable
import io.shortcut.data.source.db.dao.FavoriteDao
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.FavoriteRepository
import javax.inject.Inject

class FavoriteRepositoryImpl @Inject constructor(
    private val favoriteDao: FavoriteDao
) : FavoriteRepository {

    override fun favoriteComic(comicModel: ComicModel): Flowable<Unit> {
        return Flowable.fromCallable { favoriteDao.favoriteComic(comicModel) }
    }

    override fun unFavoriteComic(comicModel: ComicModel): Flowable<Unit> {
        return Flowable.fromCallable { favoriteDao.unFavoriteJsonFeed(comicModel.num) }
    }

    override fun isFavoriteComic(comicModel: ComicModel): Flowable<Int> {
        return Flowable.fromCallable { favoriteDao.isFavoriteComic(comicModel.num) }
    }

}