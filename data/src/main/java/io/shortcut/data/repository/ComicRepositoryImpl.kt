package io.shortcut.data.repository

import io.reactivex.Flowable
import io.shortcut.data.di.qualifier.Cloud
import io.shortcut.data.source.cloud.BaseCloudRepository
import io.shortcut.domain.model.comic.ComicModel
import io.shortcut.domain.repository.ComicRepository
import javax.inject.Inject

class ComicRepositoryImpl @Inject constructor(
    @Cloud private val cloudRepository: BaseCloudRepository,
) : ComicRepository {

    override fun getLastComic(): Flowable<ComicModel> {
        return cloudRepository.getLatestComic()
    }

    override fun getComicByNum(num: Int): Flowable<ComicModel> {
        return cloudRepository.getComicByNumber(num)
    }
}